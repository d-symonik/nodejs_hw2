const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(2)
    .max(30)
    .required(),

  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/),

  name: Joi.string()
    .alphanum()
    .min(2)
    .max(60),
});

const UserSchema = mongoose.Schema({
  name: {
    type: String,
    required: false,
  },
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
}, { timestamps: { createdAt: 'createdDate', updatedAt: false } });
module.exports = {
  UserSchema: mongoose.model('User', UserSchema),
  userJoiSchema,
};
