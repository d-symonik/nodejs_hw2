const mongoose = require('mongoose');
const Joi = require('joi');

const notesJoiSchema = Joi.object({
  userId: Joi.string()
    .alphanum()
    .required(),

  name: Joi.string()
    .alphanum()
    .min(2)
    .max(60),
});
const NoteSchema = mongoose.Schema({
  userId: {
    type: String,
    required: true,

  },
  completed: {
    type: Boolean,
    required: true,

  },
  text: {
    type: String,
    required: true,
  },
}, { timestamps: { createdAt: 'createdDate', updatedAt: false } });

module.exports = {
  NoteSchema: mongoose.model('Note', NoteSchema),
  notesJoiSchema,
};
