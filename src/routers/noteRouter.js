const express = require('express');

const router = express.Router();
const {
  addNotes, getNotes, getNotesById, updateNoteById, checkUncheckUsersNote, deleteNoteById,
} = require('../controllers/noteController');
const { authMiddleware } = require('../middlewares/authMiddleware');

router.post('/', authMiddleware, addNotes);
router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, getNotesById);
router.put('/:id', authMiddleware, updateNoteById);
router.patch('/:id', authMiddleware, checkUncheckUsersNote);
router.delete('/:id', authMiddleware, deleteNoteById);

module.exports = {
  noteRouter: router,
};
