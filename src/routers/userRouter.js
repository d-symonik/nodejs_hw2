const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middlewares/authMiddleware');
const { getUserInfo, deleteUser, updateUser } = require('../controllers/usersController');

router.get('/', authMiddleware, getUserInfo);
router.delete('/', authMiddleware, deleteUser);
router.patch('/', authMiddleware, updateUser);
module.exports = {
  usersRouter: router,
};
