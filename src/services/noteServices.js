const { NoteSchema } = require('../models/Notes');

const addNote = async ({ userId, text }) => {
  const note = new NoteSchema({
    userId,
    completed: false,
    text,
  });
  return note.save();
};
module.exports = {
  addNote,
};
