const bcryptjs = require('bcryptjs');
const { UserSchema } = require('../models/Users');

const createUser = async ({ name, username, password }) => {
  const user = new UserSchema({
    name,
    username,
    password: await bcryptjs.hash(password, 10),
  });

  return user.save();
};

module.exports = {
  createUser,
};
