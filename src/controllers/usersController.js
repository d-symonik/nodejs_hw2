const bcryptjs = require('bcryptjs');
const { UserSchema } = require('../models/Users');
/* eslint no-underscore-dangle: 0 */
function getUserInfo(req, res) {
  if (!req.user) res.status(500).end({ message: 'Internal server error' });
  /* if(!res.json({user:req.user}))  res.status(400).end({ 'message': 'Bad request'}) */
  return res.json({ user: req.user });
}

function deleteUser(req, res) {
  if (!req.user._id) {
    res.status(400).send({ message: 'Bad request' });
  }
  UserSchema.findByIdAndDelete(req.user._id)
    .then(() => {
      res.status(200).send({ message: 'Success' });
    }).catch(() => {
      res.status(500).send({ message: 'Internal server error' });
    });
}

async function updateUser(req, res) {
  const { oldPassword, newPassword } = req.body;
  const user = await UserSchema.findById(req.user._id);
  const hashPassword = await bcryptjs.hash(newPassword, 10);

  if (await bcryptjs.compare(oldPassword, user.password)) {
    await UserSchema.findByIdAndUpdate(req.user._id, { password: hashPassword })
      .then(() => {
        res.status(200).send({ message: 'Success' });
      })
      .catch(() => {
        res.status(500).send({ message: 'Internal server error' });
      });
  } else {
    res.status(400).send({ message: 'Bad request' });
  }
}

module.exports = {
  getUserInfo,
  deleteUser,
  updateUser,
};
