const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const dotenv = require('dotenv');
const { userJoiSchema, UserSchema } = require('../models/Users');
const { createUser } = require('../services/authServices');

dotenv.config();
const registerUser = async (req, res) => {
  const { name, username, password } = req.body;
  await userJoiSchema.validateAsync({ name, username, password }).catch(() => {
    res.status(400).send({ message: 'Bad request' });
  });

  await createUser({ name, username, password })
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch(() => {
      res.status(500).send({
        message: 'Internal server error',
      });
    });
};
const loginUser = async (req, res) => {
  const user = await UserSchema.findOne({ username: req.body.username })

  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = {
      username: user.username, name: user.name, userId: user.id, createdDate: user.createdDate,
    };
    const jwtToken = jwt.sign(payload, '123');
    return res.status(200).send({ message: 'Success', jwt_token: jwtToken });
  }

  return res.status(400).json({ message: 'Not authorized' });
};
module.exports = {
  registerUser,
  loginUser,
};
