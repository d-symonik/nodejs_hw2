const { NoteSchema } = require('../models/Notes');
const { addNote } = require('../services/noteServices');
/* eslint no-underscore-dangle: 0 */
async function addNotes(req, res) {
  const { text } = req.body;
  const userId = req.user._id;
  if (!text) {
    res.status(400).send({ message: 'Bad request' });
  }
  await addNote({ userId, text })
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch(() => {
      res.status(500).send({ message: 'Internal server error' });
    });
}

function getNotes(req, res) {
  const { offset, limit } = req.query;

  return NoteSchema.find({ userId: req.user._id }, '-__v').then((notes) => {
    res.json({
      offset,
      limit,
      count: notes.length,
      notes,
    });
  });
}

function getNotesById(req, res) {
  const noteID = req.params.id;
  if (!noteID) {
    res.status(400).send({ message: 'Bad request' });
  }
  return NoteSchema.findById(noteID).then((result) => {
    res.status(200).json({note:result});
  })
    .catch(() => {
      res.status(500).send({ message: 'Internal server error' });
    });
}

function updateNoteById(req, res) {
  const noteID = req.params.id;
  const { text } = req.body;
  if (!text) {
    res.status(400).send({ message: 'Bad request' });
  }
  return NoteSchema.findByIdAndUpdate(noteID, { text })
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch(() => {
      res.status(500).send({ message: 'Internal server error' });
    });
}

async function checkUncheckUsersNote(req, res) {
  const noteID = req.params.id;
  const note = await NoteSchema.findById(noteID);
  const isCompleted = !note.completed;

  if (!noteID) {
    res.status(400).send({ message: 'Bad request' });
  }
  await NoteSchema.findByIdAndUpdate(noteID, { completed: isCompleted })
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch(() => {
      res.status(500).send({ message: 'Internal server error' });
    });
}
async function deleteNoteById(req, res) {
  const noteID = req.params.id;

  if (!noteID) {
    res.status(400).send({ message: 'Bad request' });
  }
  await NoteSchema.findByIdAndDelete(noteID)
    .then((data) => {
      res.status(200).send({ message: 'Success', data });
    })
    .catch(() => {
      res.status(500).send({ message: 'Internal server error' });
    });
}

module.exports = {
  addNotes,
  getNotes,
  getNotesById,
  updateNoteById,
  checkUncheckUsersNote,
  deleteNoteById,
};
