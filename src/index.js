const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

const PORT = process.env.PORT || 8080;

mongoose.connect('mongodb+srv://d-symonik:root@cluster0.tejdm15.mongodb.net/NodeJS_HW2?retryWrites=true&w=majority');

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/userRouter');
const { noteRouter } = require('./routers/noteRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', noteRouter);

const start = () => {
  try {
    app.listen(PORT, () => {
      console.log(`Server is working on port ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
};
start();
